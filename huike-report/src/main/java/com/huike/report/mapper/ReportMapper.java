package com.huike.report.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
public interface ReportMapper {
	/**=========================================基本数据========================================*/
	/**
	 * 获取线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同金额
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Double getSalesAmount(@Param("startTime") String beginCreateTime,
						  @Param("endTime") String endCreateTime,
						  @Param("username") String username);

	/**=========================================今日简报========================================*/
	/**
	 * 获取今日新增线索数量
	 * @return
	 */
	Integer getTodayCluesNum(String username);


	/**
	 * 获取今日新增商机数量
	 * @return
	 */
	Integer getTodayBusinessNum(String username);


	/**
	 * 获取今日新增合同数量
	 * @return
	 */
	Integer getTodayContractNum(String username);


	/**
	 * 获取今日合同金额
	 * @return
	 */
	Double getTodaySalesAmount(String username);

	/**=========================================待办========================================*/
	/**
	 * 获取待跟进线索数目
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @param username
	 * @return
	 */
	Integer getTofollowedCluesNum(@Param("startTime") String beginCreateTime,
								  @Param("endTime") String endCreateTime,
								  @Param("username") String username);


	/**
	 * 获取待跟进商机数目
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @param username
	 * @return
	 */
	Integer getTofollowedBusinessNum(@Param("startTime") String beginCreateTime,
									 @Param("endTime") String endCreateTime,
									 @Param("username") String username);

	/**
	 * 获取待分配的线索数目
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	Integer getToallocatedCluesNum(@Param("startTime") String beginCreateTime,
								   @Param("endTime") String endCreateTime);


	/**
	 * 获取等待分配的商机数目
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	Integer getToallocatedBusinessNum(@Param("startTime") String beginCreateTime,
									  @Param("endTime") String endCreateTime);


	/**
	 * 获取成交合同 学科——数目
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	public List<Map<String,Object>> getSubjectStatistics(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);


	/**
	 * 统计分析--线索统计--新增线索数量折线图
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	public List<Map<String,Object>> getCluesStatistics(@Param("beginCreateTime") String beginCreateTime,@Param("endCreateTime") String endCreateTime);



	/**
	 * 统计分析--线索统计--线索转化率漏斗图--统计线索总数
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	Integer getCluesNums(@Param("beginCreateTime") String beginCreateTime,
									  @Param("endCreateTime") String endCreateTime);


	/**
	 * 统计分析--线索统计--线索转化率漏斗图--统计有效线索数
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	Integer getEffectiveCluesNums(@Param("beginCreateTime") String beginCreateTime,
						 @Param("endCreateTime") String endCreateTime);


	/**
	 * 统计分析--线索统计--线索转化率漏斗图--统计商机数
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	Integer getBusinessNums(@Param("beginCreateTime") String beginCreateTime,
								  @Param("endCreateTime") String endCreateTime);

	/**
	 * 统计分析--线索统计--线索转化率漏斗图--统计合同数
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	Integer getContractNums(@Param("beginCreateTime") String beginCreateTime,
							@Param("endCreateTime") String endCreateTime);


	/**
	 * 商机转龙虎榜
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	public List<Map<String,Object>> getBusinessChangeStatistics(@Param("beginCreateTime") String beginCreateTime,@Param("endCreateTime") String endCreateTime);

	/**
	 * 线索转换龙虎榜
	 * @param beginCreateTime
	 * @param endCreateTime
	 * @return
	 */
	public List<Map<String,Object>> getSalesStatistic(@Param("beginCreateTime") String beginCreateTime,@Param("endCreateTime") String endCreateTime);


}
