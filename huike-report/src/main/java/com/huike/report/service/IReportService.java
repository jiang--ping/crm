package com.huike.report.service;

import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.contract.domain.TbContract;
import com.huike.report.domain.vo.*;

import java.util.List;
import java.util.Map;

public interface IReportService {

    /**
     *新增客户统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public LineChartVO contractStatistics(String beginCreateTime, String endCreateTime);


    /**
     * 客户统计报表
     * @param tbContract
     * @return
     */
    public  List<TbContract> contractReportList(TbContract tbContract);

    /**
     * 销售统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public LineChartVO salesStatistics(String beginCreateTime, String endCreateTime);



    /**
     * 销售统计部门报表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> deptStatisticsList(String beginCreateTime, String endCreateTime);

    /**
     * 销售统计渠道报表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> channelStatisticsList(String beginCreateTime, String endCreateTime);
    /**
     * 销售统计归属人报表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> ownerShipStatisticsList(String beginCreateTime, String endCreateTime);



    /**
     * 渠道统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> chanelStatistics(String beginCreateTime, String endCreateTime);


    /**
     * 活动统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> activityStatistics(String beginCreateTime, String endCreateTime);



    public List<TbClue> cluesStatisticsList(TbClue clue);

    /**
     * 活动渠道统计
     * @param activity
     * @return
     */
    public List<ActivityStatisticsVo> activityStatisticsList(TbActivity activity);


    public IndexVo getIndex(IndexStatisticsVo request);


    public List<Map<String,Object>> salesStatisticsForIndex(IndexStatisticsVo request);


    /**
     * 首页基本数据展示
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    IndexBaseInfoVO getBaseInfo(String beginCreateTime, String endCreateTime);

    /**
     * 首页今日简报展示
     * @return
     */
    IndexTodayInfoVO getTodayInfo();

    /**
     * 获取待跟进线索数目
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    IndexTodoInfoVO getIodoInfo(String beginCreateTime, String endCreateTime);


    /**
     * 获取成交合同 学科——数目
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> getSubjectStatistics(String beginCreateTime, String endCreateTime);


    /**
     * 统计分析--线索统计--新增线索数量折线图
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public LineChartVO getCluesStatistics(String beginCreateTime, String endCreateTime);


    /**
     * 统计分析--线索统计--线索转化率漏斗图
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public VulnerabilityMapVo getVulnerabilityMap(String beginCreateTime, String endCreateTime);


    /**
     * 商机转化龙虎榜
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String,Object>> getBusinessChangeStatistics(String beginCreateTime, String endCreateTime);

    /**
     * 线索转龙虎榜
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String,Object>> getSalesStatistic(String beginCreateTime, String endCreateTime);
}
