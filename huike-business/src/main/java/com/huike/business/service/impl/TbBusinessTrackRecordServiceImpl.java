package com.huike.business.service.impl;


import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import com.huike.business.service.ITbBusinessTrackRecordService;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 商机跟进记录Service业务层处理
 * 
 * @author wgl
 * @date 2021-04-28
 */
@Service
public class TbBusinessTrackRecordServiceImpl implements ITbBusinessTrackRecordService {


    @Autowired
   private TbBusinessTrackRecordMapper tbBusinessTrackRecordMapper;

    @Autowired
    private TbBusinessMapper tbBusinessMapper;
    /**
     * 新增商机跟进记录表
     * @param businessTrackVo
     */
    @Override
    public void add(BusinessTrackVo businessTrackVo) {
        String username = SecurityUtils.getUsername();
        tbBusinessTrackRecordMapper.add(businessTrackVo,username);
        TbBusiness tbBusiness = new TbBusiness();
        BeanUtils.copyProperties(businessTrackVo,tbBusiness);
        tbBusiness.setId(businessTrackVo.getBusinessId());
        tbBusiness.setUpdateTime(new Date());
        tbBusiness.setStatus("2");
        tbBusinessMapper.updateTbBusiness(tbBusiness);
    }

    /**
     * 新增商机跟进记录
     * @param id
     * @return
     */
    @Override
    public List<TbBusinessTrackRecord> list(Long id) {


        return tbBusinessTrackRecordMapper.select(id);
    }
}
