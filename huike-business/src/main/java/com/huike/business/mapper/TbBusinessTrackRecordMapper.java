package com.huike.business.mapper;

import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商机跟进记录Mapper接口
 * @date 2021-04-28
 */
public interface TbBusinessTrackRecordMapper {

    /**
     * 新增商机跟进记录
     * @param businessTrackVo
     * @param createBy
     */
    void add(@Param("businessTrackVo")BusinessTrackVo businessTrackVo,@Param("createBy") String createBy);


    /**
     * 查询商机跟进记录
     * @param id
     * @return
     */
    List<TbBusinessTrackRecord> select(Long id);
}