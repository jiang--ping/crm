package com.huike.clues.service;


import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 线索跟进记录Service接口
 * 
 * @author WGL
 * @date 2022-04-19
 */
public interface ITbClueTrackRecordService {

    /**
     * 新增线索跟进记录
     * @param tbClueTrackRecord
     */
    void addITbClueTrackRecord(ClueTrackRecordVo tbClueTrackRecord);


    /**
     * 线索跟进记录查询
     * @param clueId
     * @return
     */
    List<TbClueTrackRecord> list(@Param("clueId") Long clueId);
}
