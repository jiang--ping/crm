package com.huike.clues.service.impl;


import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.clues.service.ITbClueTrackRecordService;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 线索跟进记录Service业务层处理
 * @date 2022-04-22
 */
@Service
public class TbClueTrackRecordServiceImpl implements ITbClueTrackRecordService {

    @Autowired
    private TbClueTrackRecordMapper tbClueTrackRecordMapper;

    @Autowired
    private TbClueMapper tbClueMapper;

    /**
     * 新增线索跟进记录
     * @param tbClueTrackRecord
     */
    @Override
    public void addITbClueTrackRecord(ClueTrackRecordVo tbClueTrackRecord) {
        //2. 利用工具类获取当前用户的用户名
        String createBy = SecurityUtils.getUsername();
        tbClueTrackRecordMapper.addITbClueTrackRecord(tbClueTrackRecord,createBy);
        TbClue tbClue = new TbClue();
        BeanUtils.copyProperties(tbClueTrackRecord,tbClue);
        tbClue.setId(tbClueTrackRecord.getClueId());
        tbClue.setUpdateTime(new Date());
        tbClue.setStatus("2");
        tbClueMapper.updateTbClue(tbClue);
    }

    @Override
    public List<TbClueTrackRecord> list(Long clueId) {

        return tbClueTrackRecordMapper.select(clueId);
    }
}
