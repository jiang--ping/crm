package com.huike.clues.mapper;


import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 线索跟进记录Mapper接口
 * @date 2021-04-19
 */
public interface TbClueTrackRecordMapper {


    /**
     * 新增线索跟进记录
     * @param tbClueTrackRecord
     */
    public void addITbClueTrackRecord(@Param("tbclueTrackRecord") ClueTrackRecordVo tbClueTrackRecord,
                                      @Param("createBy") String createBy);


    /**
     * 查询线索跟进记录
     * @param clueId
     * @return
     */
    List<TbClueTrackRecord> select(@Param("clueId") Long clueId);
}
