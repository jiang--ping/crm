package com.huike.web.controller.report;


import com.huike.common.core.domain.AjaxResult;
import com.huike.report.service.IReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private IReportService reportService;


    /**
     * 首页--基础数据统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/getBaseInfo")
    public AjaxResult getBaseInfo(@RequestParam("beginCreateTime") String beginCreateTime,
                                  @RequestParam("endCreateTime") String endCreateTime){
        return AjaxResult.success(reportService.getBaseInfo(beginCreateTime,endCreateTime));
    }


    /**
     * 首页--今日简报
     * @return
     */
    @GetMapping("/getTodayInfo")
    public AjaxResult getTodayInfo(){

        return AjaxResult.success(reportService.getTodayInfo());
    }


    /**
     * 待办
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/getTodoInfo")
    public AjaxResult getTodoInfo(@RequestParam("beginCreateTime") String beginCreateTime,
                                  @RequestParam("endCreateTime") String endCreateTime){

        return AjaxResult.success(reportService.getIodoInfo(beginCreateTime,endCreateTime));
    }


    /**
     * 商机转化龙虎榜
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/businessChangeStatistics")
    public AjaxResult getBusinessChangeStatistics(
            @RequestParam("beginCreateTime") String   beginCreateTime,
            @RequestParam("endCreateTime") String endCreateTime){

        List<Map<String, Object>> businessChangeStatistics = reportService.getBusinessChangeStatistics(beginCreateTime, endCreateTime);


        return AjaxResult.success(businessChangeStatistics);
    }


    /**
     * 线索转龙虎榜
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/salesStatistic")
    public AjaxResult getSalesStatistic(@RequestParam("beginCreateTime") String   beginCreateTime,
                                                  @RequestParam("endCreateTime") String endCreateTime){



        return AjaxResult.success(reportService.getSalesStatistic(beginCreateTime,endCreateTime));

    }
}